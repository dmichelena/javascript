# Javascript #

Lo que sabemos hasta ahora:

- [Imprimimos](#imprimimos)
- [Variables](#variables)
- [Tipos de datos](#tipos)
- [Estructuras de control](#if)
- [Comparaciones](#compare)
- [Conector Y](#and)
- [Conector O](#or)
- [Funciones sin retorno](#function-void)
- [Funciones con retorno](#function)

## Imprimimos

```js
let dato = "Hola";
console.log(dato);// Saldría "Hola"
console.log("dato");// Saldría "dato"
```

Consideraciones:

* El parámetro de "console.log" puede ser una variable o un valor directamente.

## Variables

```js
let nombre = "Juan";// Creando variable y asignando valor
let edad;// Creando variable
// ...
edad = 17;// Asignando valor
let saldoCuenta = 600;
let saldo_cuenta = 600;
```

Consideraciones:

- El nombre de la variable empieza en minúscula.
- Cuando es un nombre compuesto, cada palabra es separada por un "_" o cada nueva palabra empieza con una mayúscula.


## Tipos de datos

```js
let nombre = "Juan";// string - texto
let edad = 10;// number - número (int - entero)
let altura = 1.83;// number - número (double - decimal)
let esCasado = true; // boolean - boleano (true o false)

if (typeof nombre == "string") {
	// ...
}
```

Consideraciones:

- Para saber el tipo de dato de una variable se usa: typeof


## Estructuras de control

```js
//SI
if (condicion) {
	// ...
}

//SI - SINO
if (condicion) {
	// ...
} else {
	// ...
}

//SI - SINO SI
if (condicion) {
	// ...
} else if (condicion2) {
	// ...
} else if (condicion3) {
	// ...
} else if (condicion4) {
	// ...
} else {
	// ...
}
```

Consideraciones:

- Solo se va a ejecutar el código que se encuentre dentro de las llaves "{" - "}" si la condición es "true".
- Solo se va a evaluar la condicion dentro de un "else if" si la condición anterior es "false".
- Solo se va a ejecutar el código del "else" cuando la(s) condición(es) anterior(es) no se cumplan (sean "false");

## Comparaciones

```js
let numero = 10;
let mayorQue = 13 > 5;// true
let mayorOIgualQue = 10 >= 11;// false
let menorQue = 13 < 5;// false
let menorOIgualQue = 10 <= 11;// true
let igualQue = numero == 10;// true
```

## Conector Y

```js
if (condicion1 && condicion2) {
	// ...
}
```

Reglas:

- true && true == true
- true && false == false
- false && true == false
- false && false == false
- Para recordar: Solo es verdadero si los dos son verdaderos

## Conector O

```js
if (condicion1 || condicion2) {
	// ...
}
```

Reglas:

- true \|\| true == true
- true \|\| false == true
- false \|\| true == true
- false \|\| false == false
- Para recordar: Solo es false si los dos son falsos

## Funciones sin retorno

```js
function funcionPrueba(parametro1, parametro2) {
	// ...
}
functionPrueba(parametro1, parametro2);
```

Consideraciones:

- Se nombran a las funciones como se nombran a las variables.
- Puede tener N parametros.
- Cuando se llama a la función se tiene que respetar los parámetros.

## Funciones con retorno

Solo se diferencias de las anteriores en que retornan algún valor.

```js
function obtenerDia() {
	return 20;
}
let dia = obtenerDia();
```

Consideraciones:

- Probablemente sea necesario guardar el valor devuelto en una variable.
