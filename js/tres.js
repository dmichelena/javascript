console.log("1 2 3 4 5");
for (let i = 1; i < 6; i ++) {
	console.log(i);
}

// Cuando quieres que se incremente por algún valor que no sea uno se usa +=X
// i += 2 es lo mismo que poner i = i + 2
console.log("4 6 8 10");
for (let i = 4; i <= 10; i += 2) {
	console.log(i);
}

console.log("4 6 8 10");
for (let i = 2; i <= 5; i ++) {
	console.log(i * 2);
}

console.log("1 4 9 16 25 36 49 64");
for (let i = 1; i < 9; i ++) {
	console.log(i * i);
}

//TAREA
console.log("18 27 36 45 54");
console.log("9 24 39 54 69");
console.log("0 3 8 15 35 48");//Si hacen esto están en nivel DIOS
