// PARA - for
// Sirve para repeticiones
let a = 5;
console.log(a);
a++;
console.log(a);
a = a + 1;
console.log(a);
a = a + 8;//SOBREESCRIBIR
console.log(a);
console.log('---------------');

// for (desdeDonde; condicion; deCuantoEnCuantoCrece) {
//	...
// }


for (let i = 3; i < 8; i++) {
	console.log(i);
}

console.log("Terminó");

// Tiene tres partes:
// La primera hace referencia al inicio - Inicia en 0
// La segunda hace referencia a cuando sigue - Sigue hasta que i sea igual 10
// La tercera hace referencia a como crece - Crece de uno en uno
// *** cuando haces un ++ como si aumentaramos en uno el valor de la variable.