function random(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}
function tirarDado() {
	return random(1, 6);
}
function imprimir(data) {
	console.log(data);
}

let jugadorUno = tirarDado();
let jugadorDos = tirarDado();
imprimir("Jugador 1:");
imprimir(jugadorUno);
imprimir("Jugador 2:");
imprimir(jugadorDos);
if (jugadorUno > jugadorDos) {
	imprimir("Ganó el jugador 1!!!");
} else if (jugadorDos > jugadorUno) {
	imprimir("Ganó el jugador 2!!!");
} else {
	imprimir("Empataron. Los dos son perdedores!");
}