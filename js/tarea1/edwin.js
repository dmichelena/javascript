function random(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}
function tirarDado() {
	return random(1, 6);
}
function imprimir(data) {
	console.log(data);
}

let jugadorUno = tirarDado();
let jugadorDos = tirarDado();
let jugadorTres =tirarDado();
imprimir("Jugador 1:");
imprimir(jugadorUno);
imprimir("Jugador 2:");
imprimir(jugadorDos);
imprimir("Jugador 3:");
imprimir(jugadorTres);

if (jugadorUno > jugadorDos && jugadorUno > jugadorTres) {
	imprimir("Ganó el jugador 1!!!");
} else if (jugadorDos > jugadorUno && jugadorDos > jugadorTres) {
    imprimir("Ganó el jugador 2!!!");
}else if (jugadorTres > jugadorUno && jugadorTres > jugadorDos) {
	imprimir("Ganó el jugador 3!!!");
} else {
	imprimir("Empataron. Los tres son perdedores!");
}

// Faltó considerar empates